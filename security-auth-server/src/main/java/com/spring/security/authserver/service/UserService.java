package com.spring.security.authserver.service;

import com.spring.security.authserver.bean.SysUser;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    SysUser getByUsername(String username);
}
