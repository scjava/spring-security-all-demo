package com.spring.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthResourceController {

    @Autowired
    private TokenStore tokenStore;

    @RequestMapping("/getAuth")
    public Object getCurrentUser(@RequestParam("access_token") String token) {
        OAuth2Authentication oAuth2Authentication =  tokenStore.readAuthentication(token);

        return oAuth2Authentication.getUserAuthentication().getPrincipal();
    }
}
