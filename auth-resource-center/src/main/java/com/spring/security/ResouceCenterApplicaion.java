package com.spring.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResouceCenterApplicaion {

    public static void main(String[] args) {
        SpringApplication.run(ResouceCenterApplicaion.class,args);
    }
}
