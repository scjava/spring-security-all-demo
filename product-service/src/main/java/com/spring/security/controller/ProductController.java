package com.spring.security.controller;

import com.spring.security.entity.Product;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class ProductController {

    @RequestMapping("/selectProductInfoById")
    public Product getProduct(long id) {
        Product info = new Product();
        info.setId(id);
        info.setName("product " + id);
        info.setCount(1200);
        info.setType("E");
        return info;
    }
}
