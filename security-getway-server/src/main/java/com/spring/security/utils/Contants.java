package com.spring.security.utils;

public class Contants {
    public static final String clientId = "gateway-server";

    public static final String clientSecret = "123456";

    public static final String checkTokenUrl = "http://auth-server/oauth/check_token";
}
