package com.spring.security.controller;

import com.spring.security.entity.OrderInfo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {

    @RequestMapping("/selectOrderByOrderId")
    public OrderInfo getOrderInfo(long id) {
        OrderInfo info = new OrderInfo();
        info.setId(id);
        info.setName("order " + id);
        info.setPrice(555.67);
        return info;
    }
}
